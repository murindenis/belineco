<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "dealer_file".
 *
 * @property int $id
 * @property int $dealer_category_id
 * @property string $name
 * @property string $path
 * @property string $base_url
 * @property string $type
 * @property int $created_at
 *
 * @property DealerCategory $dealerCategory
 */
class DealerFile extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dealer_file';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url',
                'typeAttribute'    => 'type',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dealer_category_id', 'created_at'], 'integer'],
            [['name'], 'required'],
            [['name', 'path', 'base_url', 'type'], 'string', 'max' => 255],
            [['dealer_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DealerCategory::className(), 'targetAttribute' => ['dealer_category_id' => 'id']],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'dealer_category_id' => Yii::t('common', 'Dealer Category ID'),
            'name' => Yii::t('common', 'Name'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'type' => Yii::t('common', 'Type'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealerCategory()
    {
        return $this->hasOne(DealerCategory::className(), ['id' => 'dealer_category_id']);
    }

    public function getFilePath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
