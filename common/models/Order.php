<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $product_id
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $country
 * @property string $firm
 * @property string $position
 * @property string $email
 * @property int $created_at
 *
 * @property Product $product
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'created_at'], 'integer'],
            [['firstname', 'lastname', 'country', 'firm', 'position', 'email'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 18],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'product_id' => Yii::t('common', 'Product ID'),
            'firstname' => Yii::t('common', 'Firstname'),
            'lastname' => Yii::t('common', 'Lastname'),
            'phone' => Yii::t('common', 'Phone'),
            'country' => Yii::t('common', 'Country'),
            'firm' => Yii::t('common', 'Firm'),
            'position' => Yii::t('common', 'Position'),
            'email' => Yii::t('common', 'Email'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
