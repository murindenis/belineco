<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $product_category_id
 * @property int $product_brand_id
 * @property string $name
 * @property string $slug
 * @property string $introtext
 * @property string $characteristic
 * @property string $path
 * @property string $base_url
 * @property string $option_amount
 * @property string $option_calibr
 * @property string $option_type
 * @property string $option_color
 * @property int $created_at
 * @property boolean $index
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_h1
 *
 * @property ProductBrand $productBrand
 * @property ProductCategory $productCategory
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'name',
                'ensureUnique' => true,
                'immutable'    => true
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'created_at']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'      => Url::to('product/'.$model->slug, true),
                        'lastmod'  => date('c',$model->created_at),
                        'priority' => 0.6
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_category_id', 'product_brand_id', 'name', 'introtext'], 'required'],
            [['product_category_id', 'product_brand_id', 'created_at'], 'integer'],
            [['introtext', 'characteristic', 'seo_title', 'seo_keywords', 'seo_description', 'seo_h1'], 'string'],
            [['name', 'slug', 'path', 'base_url', 'option_amount', 'option_calibr', 'option_type', 'option_color'], 'string', 'max' => 255],
            [['product_brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductBrand::className(), 'targetAttribute' => ['product_brand_id' => 'id']],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
            [['picture'], 'safe'],
            [['index'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'product_category_id' => Yii::t('common', 'Product Category ID'),
            'product_brand_id' => Yii::t('common', 'Product Brand ID'),
            'name' => Yii::t('common', 'Name'),
            'slug' => Yii::t('common', 'Slug'),
            'introtext' => Yii::t('common', 'Introtext'),
            'characteristic' => Yii::t('common', 'Characteristic'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'option_amount' => Yii::t('common', 'Option Amount'),
            'option_calibr' => Yii::t('common', 'Option Calibr'),
            'option_type' => Yii::t('common', 'Option Type'),
            'option_color' => Yii::t('common', 'Option Color'),
            'created_at' => Yii::t('common', 'Created At'),
            'seo_keywords' => Yii::t('common', 'Keywords'),
            'seo_description' => Yii::t('common', 'Description'),
            'seo_title' => Yii::t('common', 'Title'),
            'seo_h1' => Yii::t('common', 'H1'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBrand()
    {
        return $this->hasOne(ProductBrand::className(), ['id' => 'product_brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    public function getBrandNameById($id) {
        return (ProductBrand::find()->where(['id' => $id])->one())->name;
    }

    public function getImgPath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
