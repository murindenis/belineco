<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "requisites".
 *
 * @property int $id
 * @property string $address
 * @property string $bic
 * @property string $iban
 * @property string $bank
 * @property string $address_legal
 */
class Requisites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requisites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'bic', 'iban', 'bank', 'address_legal'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address' => Yii::t('common', 'Address'),
            'bic' => Yii::t('common', 'Bic'),
            'iban' => Yii::t('common', 'Iban'),
            'bank' => Yii::t('common', 'Bank'),
            'address_legal' => Yii::t('common', 'Address Legal'),
        ];
    }
}
