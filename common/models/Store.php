<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "store".
 *
 * @property int $id
 * @property int $store_city_name_id
 * @property string $name
 * @property string $address
 * @property string $phone_mob
 * @property string $phone_home
 * @property string $fax
 * @property string $email
 * @property string $skype
 * @property string $site
 * @property int $created_at
 *
 * @property StoreCityName $storeCityName
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_city_name_id', 'name', 'address'], 'required'],
            [['store_city_name_id', 'created_at'], 'integer'],
            [['name', 'address', 'phone_mob', 'phone_home', 'fax', 'email', 'skype', 'site'], 'string', 'max' => 255],
            [['store_city_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreCityName::className(), 'targetAttribute' => ['store_city_name_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'store_city_name_id' => Yii::t('common', 'Store City Name ID'),
            'name' => Yii::t('common', 'Name'),
            'address' => Yii::t('common', 'Address'),
            'phone_mob' => Yii::t('common', 'Phone Mob'),
            'phone_home' => Yii::t('common', 'Phone Home'),
            'fax' => Yii::t('common', 'Fax'),
            'email' => Yii::t('common', 'Email'),
            'skype' => Yii::t('common', 'Skype'),
            'site' => Yii::t('common', 'Site'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCityName()
    {
        return $this->hasOne(StoreCityName::className(), ['id' => 'store_city_name_id']);
    }
}
