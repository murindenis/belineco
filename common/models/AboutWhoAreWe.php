<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "about_who_are_we".
 *
 * @property int $id
 * @property int $about_category_id
 * @property string $title
 * @property string $introtext
 * @property string $text
 * @property string $path
 * @property string $base_url
 * @property int $created_at
 *
 * @property AboutCategory $aboutCategory
 */
class AboutWhoAreWe extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_who_are_we';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_category_id', 'title', 'introtext', 'text'], 'required'],
            [['about_category_id', 'created_at'], 'integer'],
            [['introtext', 'text'], 'string'],
            [['title', 'path', 'base_url'], 'string', 'max' => 255],
            [['about_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCategory::className(), 'targetAttribute' => ['about_category_id' => 'id']],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'about_category_id' => Yii::t('common', 'About Category ID'),
            'title' => Yii::t('common', 'Title'),
            'introtext' => Yii::t('common', 'Introtext'),
            'text' => Yii::t('common', 'Text'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCategory()
    {
        return $this->hasOne(AboutCategory::className(), ['id' => 'about_category_id']);
    }

    public function getImgPath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
