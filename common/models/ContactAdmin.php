<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "contact_admin".
 *
 * @property int $id
 * @property string $position
 * @property string $name
 * @property string $phone_mob
 * @property string $phone_home
 * @property string $fax
 * @property string $email
 * @property string $skype
 * @property int $created_at
 */
class ContactAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_admin';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'name'], 'required'],
            [['created_at'], 'integer'],
            [['position', 'name', 'phone_mob', 'phone_home', 'fax', 'email', 'skype'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'position' => Yii::t('common', 'Position'),
            'name' => Yii::t('common', 'Name'),
            'phone_mob' => Yii::t('common', 'Phone Mob'),
            'phone_home' => Yii::t('common', 'Phone Home'),
            'fax' => Yii::t('common', 'Fax'),
            'email' => Yii::t('common', 'Email'),
            'skype' => Yii::t('common', 'Skype'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }
}
