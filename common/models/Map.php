<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "map".
 *
 * @property int $id
 * @property string $name
 * @property string $base_url
 * @property string $path
 */
class Map extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'map';
    }

    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'base_url', 'path'], 'string', 'max' => 255],
            [['picture'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'base_url' => 'Base Url',
            'path' => 'Path',
        ];
    }

    public function getImgPath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
