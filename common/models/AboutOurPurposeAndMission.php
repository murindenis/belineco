<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "about_our_purpose_and_mission".
 *
 * @property int $id
 * @property int $about_category_id
 * @property string $title
 * @property string $introtext
 * @property string $text
 * @property int $created_at
 *
 * @property AboutCategory $aboutCategory
 */
class AboutOurPurposeAndMission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_our_purpose_and_mission';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['about_category_id', 'title', 'introtext', 'text'], 'required'],
            [['about_category_id', 'created_at'], 'integer'],
            [['introtext', 'text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['about_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCategory::className(), 'targetAttribute' => ['about_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'about_category_id' => Yii::t('common', 'About Category ID'),
            'title' => Yii::t('common', 'Title'),
            'introtext' => Yii::t('common', 'Introtext'),
            'text' => Yii::t('common', 'Text'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCategory()
    {
        return $this->hasOne(AboutCategory::className(), ['id' => 'about_category_id']);
    }

}
