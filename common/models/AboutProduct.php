<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "about_product".
 *
 * @property int $id
 * @property string $title
 * @property int $about_category_id
 * @property string $path
 * @property string $base_url
 * @property int $created_at
 *
 * @property AboutCategory $aboutCategory
 */
class AboutProduct extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about_product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'about_category_id'], 'required'],
            [['about_category_id', 'created_at'], 'integer'],
            [['title', 'path', 'base_url'], 'string', 'max' => 255],
            [['about_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AboutCategory::className(), 'targetAttribute' => ['about_category_id' => 'id']],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'about_category_id' => Yii::t('common', 'About Category ID'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutCategory()
    {
        return $this->hasOne(AboutCategory::className(), ['id' => 'about_category_id']);
    }

    public function getImgPath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
