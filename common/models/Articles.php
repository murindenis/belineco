<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $introtext
 * @property string $description
 * @property string $path
 * @property string $base_url
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_h1
 * @property int $created_at
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'title',
                'ensureUnique' => true,
                'immutable'    => true
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'created_at']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'      => Url::to('articles/'.$model->slug, true),
                        'lastmod'  => date('c',$model->created_at),
                        'priority' => 0.6
                    ];
                }
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute'        => 'picture',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'introtext', 'description'], 'required'],
            [['introtext', 'description', 'seo_description'], 'string'],
            [['created_at'], 'integer'],
            [['title', 'slug', 'path', 'base_url', 'seo_title', 'seo_keywords', 'seo_h1'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => 'Название',
            'slug' => Yii::t('common', 'Slug'),
            'introtext' => Yii::t('common', 'Introtext'),
            'description' => 'Описание',
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    public function getImgPath() {
        if ($this->path && $this->base_url) {
            return $this->base_url . '/' . $this->path;
        }
        return false;
    }
}
