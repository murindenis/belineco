<?php

namespace common\models;

use himiklab\sitemap\behaviors\SitemapBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $created_at
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_h1
 * @property int $sort
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'name',
                'ensureUnique' => true,
                'immutable'    => true
            ],
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['slug', 'created_at']);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc'      => Url::to('category/'.$model->slug, true),
                        'lastmod'  => date('c',$model->created_at),
                        'priority' => 0.8
                    ];
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug'], 'required'],
            [['created_at', 'sort'], 'integer'],
            [['name', 'slug', 'seo_title', 'seo_description', 'seo_keywords', 'seo_h1'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'slug' => Yii::t('common', 'Slug'),
            'created_at' => Yii::t('common', 'Created At'),
            'seo_keywords' => Yii::t('common', 'Keywords'),
            'seo_description' => Yii::t('common', 'Description'),
            'seo_title' => Yii::t('common', 'Title'),
            'seo_h1' => Yii::t('common', 'H1'),
            'sort' => 'Порядок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBrands()
    {
        return $this->hasMany(ProductBrand::className(), ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_category_id' => 'id']);
    }
}
