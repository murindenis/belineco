<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `product_brand`.
 */
class m180322_110445_add_photo_columns_to_product_brand_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_brand', 'path', $this->string()->after('name'));
        $this->addColumn('product_brand', 'base_url', $this->string()->after('path'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_brand', 'path');
        $this->dropColumn('product_brand', 'base_url');
    }
}
