<?php

use yii\db\Migration;

/**
 * Handles the creation of table `what_do_we_offer`.
 */
class m180321_130415_create_about_what_do_we_offer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_what_do_we_offer', [
            'id'         => $this->primaryKey(),
            'about_category_id' => $this->integer()->notNull(),
            'title'      => $this->string()->notNull(),
            'introtext'  => $this->text()->notNull(),
            'text'       => $this->text()->notNull(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-about_what_do_we_offer_about_category_id', '{{%about_what_do_we_offer}}', 'about_category_id');
        $this->addForeignKey('fk-about_what_do_we_offer_about_category_id', '{{%about_what_do_we_offer}}', 'about_category_id', '{{%about_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_what_do_we_offer_about_category_id','about_what_do_we_offer');
        $this->dropIndex('idx-about_what_do_we_offer_about_category_id','about_what_do_we_offer');
        $this->dropTable('about_what_do_we_offer');
    }
}
