<?php

use yii\db\Migration;

/**
 * Handles adding index to table `product`.
 */
class m180326_075537_add_index_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'index', $this->boolean()->defaultValue(false)->comment('Показать в слайдере на главной'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'index');
    }
}
