<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m180326_145243_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('articles', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string()->notNull(),
            'slug'        => $this->string()->notNull(),
            'introtext'   => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'path'        => $this->string(),
            'base_url'    => $this->string(),
            'seo_title'       => $this->string(),
            'seo_keywords'    => $this->string(),
            'seo_description' => $this->text(),
            'seo_h1'          => $this->string(),
            'created_at'  => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}
