<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_option`.
 */
class m180322_103735_create_product_option_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('product_option', [
            'id'    => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'name'  => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-product_option_product_id', '{{%product_option}}', 'product_id');
        $this->addForeignKey('fk-product_option_product_id', '{{%product_option}}', 'product_id', '{{%product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_option_product_id','product_option');
        $this->dropIndex('idx-product_option_product_id','product_option');
        $this->dropTable('product_option');
    }
}
