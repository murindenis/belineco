<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requisites`.
 */
class m180322_120820_create_requisites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('requisites', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->notNull(),
            'bic'     => $this->string(),
            'iban'    => $this->string(),
            'bank'    => $this->string(),
            'address_legal' => $this->string(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('requisites');
    }
}
