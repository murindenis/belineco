<?php

use yii\db\Migration;

/**
 * Handles the creation of table `region`.
 */
class m180323_062157_create_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->insert('region', ['name' => 'Брестская область']);
        $this->insert('region', ['name' => 'Витебская область']);
        $this->insert('region', ['name' => 'Гомельская область']);
        $this->insert('region', ['name' => 'Гродненская область']);
        $this->insert('region', ['name' => 'Минская область']);
        $this->insert('region', ['name' => 'Могилевская область']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('region');
    }
}
