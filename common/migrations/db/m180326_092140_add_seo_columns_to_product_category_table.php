<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `product_category`.
 */
class m180326_092140_add_seo_columns_to_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_category', 'seo_title', $this->string());
        $this->addColumn('product_category', 'seo_keywords', $this->string());
        $this->addColumn('product_category', 'seo_description', $this->string());
        $this->addColumn('product_category', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'seo_h1');
        $this->dropColumn('product_category', 'seo_description');
        $this->dropColumn('product_category', 'seo_keywords');
        $this->dropColumn('product_category', 'seo_title');
    }
}
