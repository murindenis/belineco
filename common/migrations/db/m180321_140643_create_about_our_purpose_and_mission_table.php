<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_our_purpose_and_mission`.
 */
class m180321_140643_create_about_our_purpose_and_mission_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_our_purpose_and_mission', [
            'id'         => $this->primaryKey(),
            'about_category_id' => $this->integer()->notNull(),
            'title'      => $this->string()->notNull(),
            'introtext'  => $this->text()->notNull(),
            'text'       => $this->text()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-about_our_purpose_and_mission_about_category_id', '{{%about_our_purpose_and_mission}}', 'about_category_id');
        $this->addForeignKey('fk-about_our_purpose_and_mission_about_category_id', '{{%about_our_purpose_and_mission}}', 'about_category_id', '{{%about_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_our_purpose_and_mission_about_category_id','about_our_purpose_and_mission');
        $this->dropIndex('idx-about_our_purpose_and_mission_about_category_id','about_our_purpose_and_mission');
        $this->dropTable('about_our_purpose_and_mission');
    }
}
