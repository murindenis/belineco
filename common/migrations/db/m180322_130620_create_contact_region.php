<?php

use yii\db\Migration;

/**
 * Class m180322_130620_create_contact_region
 */
class m180322_130620_create_contact_region extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contact_region', [
            'id'         => $this->primaryKey(),
            'position'   => $this->string(),
            'name'       => $this->string(),
            'phone_mob'  => $this->string(),
            'phone_home' => $this->string(),
            'fax'        => $this->string(),
            'email'      => $this->string(),
            'skype'      => $this->string(),
            'created_at' => $this->integer(),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('contact_region');
    }
}
