<?php

use yii\db\Migration;

/**
 * Handles adding address to table `contact_region_name`.
 */
class m180322_125355_add_address_column_to_contact_region_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contact_region_name', 'address', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contact_region_name','address');
    }
}
