<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_category`.
 */
class m180321_110318_create_about_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_category', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('about_category');
    }
}
