<?php

use yii\db\Migration;

/**
 * Handles adding option_color to table `product`.
 */
class m180323_102636_add_option_color_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'option_color', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'option_color');
    }
}
