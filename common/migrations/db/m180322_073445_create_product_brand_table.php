<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_brend`.
 */
class m180322_073445_create_product_brand_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('product_brand', [
            'id'         => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-product_brand_product_category_id', '{{%product_brand}}', 'product_category_id');
        $this->addForeignKey('fk-product_brand_product_category_id', '{{%product_brand}}', 'product_category_id', '{{%product_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_brand_product_category_id','product_brand');
        $this->dropIndex('idx-product_brand_product_category_id','product_brand');
        $this->dropTable('product_brand');
    }
}
