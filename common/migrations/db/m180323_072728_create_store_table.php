<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store`.
 */
class m180323_072728_create_store_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('store', [
            'id' => $this->primaryKey(),
            'store_city_name_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'phone_mob'  => $this->string(),
            'phone_home' => $this->string(),
            'fax'        => $this->string(),
            'email'      => $this->string(),
            'skype'      => $this->string(),
            'site'       => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-store_store_city_name_id', '{{%store}}', 'store_city_name_id');
        $this->addForeignKey('fk-store_store_city_name_id', '{{%store}}', 'store_city_name_id', '{{%store_city_name}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-store_store_city_name_id','store');
        $this->dropIndex('idx-store_store_city_name_id','store');
        $this->dropTable('store');
    }
}
