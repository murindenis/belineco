<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_photo`.
 */
class m180322_060918_create_about_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_photo', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string()->notNull(),
            'about_category_id' => $this->integer()->notNull(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-about_photo_about_category_id', '{{%about_photo}}', 'about_category_id');
        $this->addForeignKey('fk-about_photo_about_category_id', '{{%about_photo}}', 'about_category_id', '{{%about_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_photo_about_category_id','about_photo');
        $this->dropIndex('idx-about_photo_about_category_id','about_photo');
        $this->dropTable('about_photo');
    }
}
