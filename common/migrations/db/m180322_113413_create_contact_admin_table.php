<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m180322_113413_create_contact_admin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contact_admin', [
            'id'         => $this->primaryKey(),
            'position'   => $this->string()->notNull(),
            'name'       => $this->string()->notNull(),
            'phone_mob'  => $this->string(),
            'phone_home' => $this->string(),
            'fax'        => $this->string(),
            'email'      => $this->string(),
            'skype'      => $this->string(),
            'created_at' => $this->integer(),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contact_admin');
    }
}
