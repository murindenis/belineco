<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email`.
 */
class m180525_105011_create_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('email', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
        ], $tableOptions);
        $this->insert('email',['email' => 'export@belineco.com']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('email');
    }
}
