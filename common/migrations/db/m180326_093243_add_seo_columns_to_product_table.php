<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `product`.
 */
class m180326_093243_add_seo_columns_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'seo_title', $this->string());
        $this->addColumn('product', 'seo_keywords', $this->string());
        $this->addColumn('product', 'seo_description', $this->string());
        $this->addColumn('product', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'seo_h1');
        $this->dropColumn('product', 'seo_description');
        $this->dropColumn('product', 'seo_keywords');
        $this->dropColumn('product', 'seo_title');
    }
}
