<?php

use yii\db\Migration;

/**
 * Handles the creation of table `seo`.
 */
class m180326_082546_create_seo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('seo', [
            'id' => $this->primaryKey(),
            'page'        => $this->string(),
            'title'       => $this->string(),
            'keywords'    => $this->string(),
            'description' => $this->string(),
            'h1'          => $this->string(),
            'priority'    => $this->string(),
            'created_at'  => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('seo');
    }
}
