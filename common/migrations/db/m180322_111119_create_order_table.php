<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180322_111119_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('order', [
            'id'         => $this->primaryKey(),
            'product_id' => $this->integer(),
            'firstname'  => $this->string(),
            'lastname'   => $this->string(),
            'phone'      => $this->string(18),
            'country'    => $this->string(),
            'firm'       => $this->string(),
            'position'   => $this->string(),
            'email'      => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-order_product_id', '{{%order}}', 'product_id');
        $this->addForeignKey('fk-order_product_id', '{{%order}}', 'product_id', '{{%product}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_product_id','order');
        $this->dropIndex('idx-order_product_id','order');
        $this->dropTable('order');
    }
}
