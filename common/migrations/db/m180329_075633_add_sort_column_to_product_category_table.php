<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `product_category`.
 */
class m180329_075633_add_sort_column_to_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_category', 'sort', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'sort');
    }
}
