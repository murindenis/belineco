<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact_region_name`.
 */
class m180322_124010_create_contact_region_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contact_region_name', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contact_region_name');
    }
}
