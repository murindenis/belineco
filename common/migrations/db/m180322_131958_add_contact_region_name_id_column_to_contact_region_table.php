<?php

use yii\db\Migration;

/**
 * Handles adding contact_region_name_id to table `contact_region`.
 */
class m180322_131958_add_contact_region_name_id_column_to_contact_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contact_region', 'contact_region_name_id', $this->integer()->notNull());

        $this->createIndex('idx-contact_region_contact_region_name_id', '{{%contact_region}}', 'contact_region_name_id');
        $this->addForeignKey('fk-contact_region_contact_region_name_id', '{{%contact_region}}', 'contact_region_name_id', '{{%contact_region_name}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contact_region_contact_region_name_id','contact_region');
        $this->dropIndex('idx-contact_region_contact_region_name_id','contact_region');
        $this->dropColumn('contact_region', 'contact_region_name_id');
    }
}
