<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_who_are_we`.
 */
class m180321_115328_create_about_who_are_we_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_who_are_we', [
            'id'         => $this->primaryKey(),
            'about_category_id' => $this->integer()->notNull(),
            'title'      => $this->string()->notNull(),
            'introtext'  => $this->text()->notNull(),
            'text'       => $this->text()->notNull(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-about_who_are_we_about_category_id', '{{%about_who_are_we}}', 'about_category_id');
        $this->addForeignKey('fk-about_who_are_we_about_category_id', '{{%about_who_are_we}}', 'about_category_id', '{{%about_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_who_are_we_about_category_id','about_who_are_we');
        $this->dropIndex('fk-about_who_are_we_about_category_id','about_who_are_we');
        $this->dropTable('about_who_are_we');
    }
}
