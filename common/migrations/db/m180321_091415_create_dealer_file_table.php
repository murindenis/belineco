<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dealer_file`.
 */
class m180321_091415_create_dealer_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('dealer_file', [
            'id'         => $this->primaryKey(),
            'dealer_category_id' => $this->integer(),
            'name'       => $this->string()->notNull(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'type'       => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-dealer_file_dealer_category_id', '{{%dealer_file}}', 'dealer_category_id');
        $this->addForeignKey('fk-dealer_file_dealer_category_id', '{{%dealer_file}}', 'dealer_category_id', '{{%dealer_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-dealer_file_dealer_category_id', 'dealer_file');
        $this->dropIndex('idx-dealer_file_dealer_category_id', 'dealer_file');
        $this->dropTable('dealer_file');
    }
}
