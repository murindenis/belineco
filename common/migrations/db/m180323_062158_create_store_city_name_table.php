<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_city_name`.
 */
class m180323_062158_create_store_city_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('store_city_name', [
            'id'         => $this->primaryKey(),
            'region_id'  => $this->integer()->notNull(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-store_city_name_region_id', '{{%store_city_name}}', 'region_id');
        $this->addForeignKey('fk-store_city_name_region_id', '{{%store_city_name}}', 'region_id', '{{%region}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-store_city_name_region_id','store_city_name');
        $this->dropIndex('idx-store_city_name_region_id','store_city_name');
        $this->dropTable('store_city_name');
    }
}
