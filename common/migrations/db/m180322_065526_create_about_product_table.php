<?php

use yii\db\Migration;

/**
 * Handles the creation of table `about_product`.
 */
class m180322_065526_create_about_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('about_product', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string()->notNull(),
            'about_category_id' => $this->integer()->notNull(),
            'path'       => $this->string(),
            'base_url'   => $this->string(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-about_product_about_category_id', '{{%about_product}}', 'about_category_id');
        $this->addForeignKey('fk-about_product_about_category_id', '{{%about_product}}', 'about_category_id', '{{%about_category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-about_product_about_category_id','about_product');
        $this->dropIndex('idx-about_product_about_category_id','about_product');
        $this->dropTable('about_product');
    }
}
