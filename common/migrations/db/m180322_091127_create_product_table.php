<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180322_091127_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('product', [
            'id'            => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'product_brand_id'    => $this->integer()->notNull(),
            'name'          => $this->string()->notNull(),
            'slug'          => $this->string(),
            'introtext'     => $this->text()->notNull(),
            'characteristic'=> $this->text(),
            'path'          => $this->string(),
            'base_url'      => $this->string(),
            'option_amount' => $this->string(),
            'option_calibr' => $this->string(),
            'option_type'   => $this->string(),
            'created_at'    => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-product_product_category_id', '{{%product}}', 'product_category_id');
        $this->addForeignKey('fk-product_product_category_id', '{{%product}}', 'product_category_id', '{{%product_category}}', 'id', 'cascade', 'cascade');

        $this->createIndex('idx-product_product_brand_id', '{{%product}}', 'product_brand_id');
        $this->addForeignKey('fk-product_product_brand_id', '{{%product}}', 'product_brand_id', '{{%product_brand}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_product_brand_id','product_brand');
        $this->dropIndex('idx-product_product_brand_id','product_brand');

        $this->dropForeignKey('fk-product_product_category_id','product_category');
        $this->dropIndex('idx-product_product_category_id','product_category');
        $this->dropTable('product');
    }
}
