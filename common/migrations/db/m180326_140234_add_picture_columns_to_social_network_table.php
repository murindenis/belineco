<?php

use yii\db\Migration;

/**
 * Handles adding picture to table `social_network`.
 */
class m180326_140234_add_picture_columns_to_social_network_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('social_network', 'path', $this->string());
        $this->addColumn('social_network', 'base_url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('social_network', 'path');
        $this->dropColumn('social_network', 'base_url');
    }
}
