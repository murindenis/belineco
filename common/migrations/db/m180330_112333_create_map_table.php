<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map`.
 */
class m180330_112333_create_map_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('map', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'base_url' => $this->string(),
            'path' => $this->string(),
        ], $tableOptions);

        $this->insert('map', ['name' => 'Схема проезда']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('map');
    }
}
