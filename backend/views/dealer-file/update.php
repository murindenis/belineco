<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DealerFile */

$this->title = 'Редактировать документ';
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать документ';
?>
<div class="dealer-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
