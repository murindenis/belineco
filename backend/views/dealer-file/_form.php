<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealerFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dealer-file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dealer_category_id')
        ->hiddenInput(['value' => Yii::$app->request->get('dcid')])->label(false) ?>

    <?= $form->field($model, 'name')->textInput()->label('Название документа') ?>

    <?php echo $form->field($model, 'picture')->widget(
        \trntv\filekit\widget\Upload::classname(),
        [
            'url' => ['avatar-upload']
        ]
    )->label('Документ') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
