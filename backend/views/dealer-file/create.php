<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DealerFile */

$this->title = 'Добавить документ';
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-file-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
