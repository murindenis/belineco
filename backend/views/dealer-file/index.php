<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DealerFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dealer Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-file-index">

    <p>
        <?= Html::a('Create Dealer File', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'dealer_category_id',
            'name',
            'path',
            'base_url:url',
            'type',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
