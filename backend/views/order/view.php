<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите  удалить этот заказ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product.name',
                'label' => 'Продукт'
            ],
            [
                'attribute' => 'firstname',
                'label' => 'Имя'
            ],
            [
                'attribute' => 'lastname',
                'label' => 'Фамилия'
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон'
            ],
            [
                'attribute' => 'country',
                'label' => 'Страна'
            ],
            [
                'attribute' => 'firm',
                'label' => 'Фирма'
            ],
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            'email:email',
            'created_at:datetime',
        ],
    ]) ?>

</div>
