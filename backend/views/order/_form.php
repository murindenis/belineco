<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->textInput() ?>

    <?= $form->field($model, 'firstname')->textInput()->label('Имя') ?>

    <?= $form->field($model, 'lastname')->textInput()->label('Фамилия') ?>

    <?= $form->field($model, 'phone')->textInput()->label('Телефон') ?>

    <?= $form->field($model, 'country')->textInput()->label('Страна') ?>

    <?= $form->field($model, 'firm')->textInput()->label('Фирма') ?>

    <?= $form->field($model, 'position')->textInput()->label('Должность') ?>

    <?= $form->field($model, 'email')->textInput()->label('Email') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
