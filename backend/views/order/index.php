<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <p>
        <?= Html::a('Добавить заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'product.name',
                'label' => 'Продукт'
            ],
            [
                'attribute' => 'firstname',
                'label' => 'Имя'
            ],
            [
                'attribute' => 'lastname',
                'label' => 'Фамилия'
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон'
            ],
            [
                'attribute' => 'country',
                'label' => 'Страна'
            ],
            [
                'attribute' => 'firm',
                'label' => 'Фирма'
            ],
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            'email:email',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
