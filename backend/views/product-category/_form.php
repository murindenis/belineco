<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-category-form">

    <div class="panel panel-info panel-base">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="panel panel-info panel-seo">
        <div class="panel-heading">SEO</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>

            <?= $form->field($model, 'seo_keywords')->textInput()->label('Keywords') ?>

            <?= $form->field($model, 'seo_description')->textarea(['rows' => '4'])->label('Description') ?>

            <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
