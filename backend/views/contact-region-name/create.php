<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactRegionName */

$this->title = 'Добавить регион';
$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-region-name-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
