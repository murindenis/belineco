<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactRegionName */

$this->title = 'Редактировать регион';
$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="contact-region-name-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
