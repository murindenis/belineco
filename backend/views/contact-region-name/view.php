<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactRegionName */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-region-name-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот регион со всеми представителями?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name',
                'label' => 'Страна / Область / Город'
            ],
            [
                'attribute' => 'address',
                'label' => 'Региональный склад'
            ],
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Представители</h3>
    <p>
        <?= Html::a('Добавить представителя', ['contact-region/create', 'crnid' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            [
                'attribute' => 'name',
                'label' => 'ФИО'
            ],
            [
                'attribute' => 'phone_mob',
                'label' => 'Мобильный'
            ],
            [
                'attribute' => 'phone_home',
                'label' => 'Телефон'
            ],
            'fax',
            'email:email',
            'skype',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'contact-region'
            ],
        ],
    ]); ?>

</div>
