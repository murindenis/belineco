<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AboutWhoAreWeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'About Who Are Wes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-who-are-we-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'about_category_id',
            'title',
            'introtext:ntext',
            //'text:ntext',
            'path',
            'base_url:url',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
