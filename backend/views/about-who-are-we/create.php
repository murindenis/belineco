<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutWhoAreWe */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-who-are-we-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
