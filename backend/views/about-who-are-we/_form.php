<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AboutWhoAreWe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-who-are-we-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'about_category_id')->hiddenInput(['value' => Yii::$app->request->get('acid')])->label(false) ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'introtext')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6])->label('Текст') ?>

    <?php echo $form->field($model, 'picture')->widget(
        \trntv\filekit\widget\Upload::classname(),
        [
            'url' => ['avatar-upload']
        ]
    )->label('Картинка') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
