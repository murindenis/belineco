<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutWhoAreWe */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-who-are-we-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
