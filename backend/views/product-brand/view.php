<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductBrand */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Бренды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-brand-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить бренд со всеми товарами?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_category_id',
            'name',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Товары</h3>
    <p>
        <?= Html::a('Добавить товар', ['/product/create', 'pbid' => $model->id, 'pcid' => $model->product_category_id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'slug',
            [
                'attribute' => 'index',
                'label' => 'Показать на главной',
                'value' => function ($data) {
                    if ($data->index) {
                        return 'Да';
                    } else {
                        return 'Нет';
                    }
                }
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'product'
            ],
        ],
    ]); ?>

</div>
