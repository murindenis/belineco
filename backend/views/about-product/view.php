<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AboutProduct */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Наши продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-product-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'about_category_id',
            'path',
            'base_url:url',
            'created_at:datetime',
        ],
    ]) ?>

</div>
