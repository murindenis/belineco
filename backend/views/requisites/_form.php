<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Requisites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="requisites-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'address_legal')->textInput()->label('Юридический адрес') ?>

    <?= $form->field($model, 'bic')->textInput()->label('BIC') ?>

    <?= $form->field($model, 'iban')->textInput()->label('IBAN') ?>

    <?= $form->field($model, 'bank')->textInput()->label('Банк') ?>

    <?= $form->field($model, 'address')->textInput()->label('Адрес') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
