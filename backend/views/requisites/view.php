<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Requisites */

$this->title = 'Реквизиты';
$this->params['breadcrumbs'][] = ['label' => 'Реквизиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?/*= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'address_legal',
                'label' => 'юридический адрес',
            ],
            'bic',
            'iban',
            [
                'attribute' => 'bank',
                'label' => 'банк',
            ],
            [
                'attribute' => 'address',
                'label' => 'адрес',
            ],
        ],
    ]) ?>

</div>
