<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Requisites */

$this->title = 'Редактировать реквизиты';
$this->params['breadcrumbs'][] = ['label' => 'Реквизиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="requisites-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
