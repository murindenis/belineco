<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RequisitesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Реквизиты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-index">
<!--
    <p>
        <?/*= Html::a('Добавить реквизиты', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'address_legal',
                'label' => 'юридический адрес',
            ],
            'bic',
            'iban',
            [
                'attribute' => 'bank',
                'label' => 'банк',
            ],
            [
                'attribute' => 'address',
                'label' => 'адрес',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]); ?>
</div>
