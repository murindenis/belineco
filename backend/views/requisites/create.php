<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Requisites */

$this->title = 'Добавить реквизиты';
$this->params['breadcrumbs'][] = ['label' => 'Реквизиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requisites-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
