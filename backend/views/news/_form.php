<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <div class="panel panel-info panel-base">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'introtext')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'description')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                    'options'=> [
                        'minHeight'   => 250,
                        'maxHeight'   => 600,
                        'buttonSource'=> true,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            )->label('Описание') ?>

            <?php echo $form->field($model, 'picture')->widget(
                \trntv\filekit\widget\Upload::classname(),
                [
                    'url' => ['avatar-upload']
                ]
            )->label('Картинка') ?>
        </div>
    </div>

    <div class="panel panel-info panel-seo">
        <div class="panel-heading">SEO</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>

            <?= $form->field($model, 'seo_keywords')->textInput()->label('Keywords') ?>

            <?= $form->field($model, 'seo_description')->textarea(['rows' => '4'])->label('Description') ?>

            <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
