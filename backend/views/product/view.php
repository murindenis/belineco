<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить товар?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_category_id',
            'product_brand_id',
            'name',
            'slug',
            'introtext:ntext',
            [
                'attribute' => 'characteristic',
                'label' => 'Хариктеристики',
                'format'=>'raw',
            ],
            'path',
            'base_url:url',
            [
                'attribute' => 'option_amount',
                'label' => 'Объем'
            ],
            [
                'attribute' => 'option_calibr',
                'label' => 'Калибр'
            ],
            [
                'attribute' => 'option_type',
                'label' => 'Тип'
            ],
            [
                'attribute' => 'option_color',
                'label' => 'Цвет'
            ],
            [
                'attribute' => 'index',
                'label' => 'Показать на главной'
            ],
            'seo_title',
            'seo_keywords',
            'seo_description',
            'seo_h1',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Опции</h3>
    <p>
        <?= Html::a('Добавить опцию', ['/product-option/create', 'pid' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'value',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'product-option'
            ],
        ],
    ]); ?>

</div>
