<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <div class="panel panel-info panel-base">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->field($model, 'picture')->widget(
                \trntv\filekit\widget\Upload::classname(),
                [
                    'url' => ['avatar-upload']
                ]
            )->label('Картинка') ?>

            <?= $form->field($model, 'product_category_id')->hiddenInput(['value' => Yii::$app->request->get('pcid')])->label(false) ?>

            <?= $form->field($model, 'product_brand_id')->hiddenInput(['value' => Yii::$app->request->get('pbid')])->label(false) ?>

            <?= $form->field($model, 'name')->textInput() ?>

            <?= $form->field($model, 'introtext')->textarea(['rows' => 4]) ?>

            <?= $form->field($model, 'characteristic')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video', 'table'],
                    'options'=> [
                        'minHeight'   => 250,
                        'maxHeight'   => 600,
                        'buttonSource'=> true,
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            )->label('Характиристика') ?>

            <?= $form->field($model, 'option_amount')->textInput()->label('Объем') ?>

            <?= $form->field($model, 'option_calibr')->textInput()->label('Калибр') ?>

            <?= $form->field($model, 'option_type')->textInput()->label('Тип') ?>

            <?= $form->field($model, 'option_color')->textInput()->label('Цвет') ?>

            <?= $form->field($model, 'index')->checkbox(['label' =>''])->label('Показать на главной') ?>
        </div>
    </div>

    <div class="panel panel-info panel-seo">
        <div class="panel-heading">SEO</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput()->label('Title') ?>

            <?= $form->field($model, 'seo_keywords')->textInput()->label('Keywords') ?>

            <?= $form->field($model, 'seo_description')->textarea(['rows' => '4'])->label('Description') ?>

            <?= $form->field($model, 'seo_h1')->textInput()->label('H1') ?>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
