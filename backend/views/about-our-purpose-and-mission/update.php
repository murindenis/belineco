<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutOurPurposeAndMission */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Цель и миссия', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-our-purpose-and-mission-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
