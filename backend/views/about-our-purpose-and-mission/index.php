<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AboutOurPurposeAndMissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цель и миссия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-our-purpose-and-mission-index">

    <p>
        <?= Html::a('Create About Our Purpose And Mission', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'about_category_id',
            'title',
            'introtext:ntext',
            //'text:ntext',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
