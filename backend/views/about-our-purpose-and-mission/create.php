<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutOurPurposeAndMission */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Цель и миссия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-our-purpose-and-mission-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
