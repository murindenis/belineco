<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AboutOurPurposeAndMission */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Цель и миссия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-our-purpose-and-mission-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?/*= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'about_category_id',
            'title',
            'introtext:ntext',
            'text:ntext',
            'created_at:datetime',
        ],
    ]) ?>

</div>
