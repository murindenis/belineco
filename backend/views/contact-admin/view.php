<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactAdmin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Администрация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-admin-view">

    <p>
        <?= Html::a('Редактировать', ['Update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['Delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            [
                'attribute' => 'name',
                'label' => 'ФИО'
            ],
            [
                'attribute' => 'phone_mob',
                'label' => 'Мобильный'
            ],
            [
                'attribute' => 'phone_home',
                'label' => 'Телефон'
            ],
            'fax',
            'email:email',
            'skype',
            'created_at:datetime',
        ],
    ]) ?>

</div>
