<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Администрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-admin-index">

    <p>
        <?= Html::a('Добавить контакт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            [
                'attribute' => 'name',
                'label' => 'ФИО'
            ],
            [
                'attribute' => 'phone_mob',
                'label' => 'Мобильный'
            ],
            [
                'attribute' => 'phone_home',
                'label' => 'Телефон'
            ],
            'fax',
            'email:email',
            'skype',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
