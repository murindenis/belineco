<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactAdmin */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Администрация', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="contact-admin-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
