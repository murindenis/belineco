<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactAdmin */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Администрация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-admin-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
