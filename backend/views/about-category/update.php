<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutCategory */

$this->title = 'Редактировать раздел';
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать раздел';
?>
<div class="about-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
