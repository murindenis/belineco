<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutCategory */

$this->title = 'Добавить раздел';
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
