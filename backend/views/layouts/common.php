<?php
/**
 * @var $this yii\web\View
 */

use backend\assets\BackendAsset;
use backend\models\SystemLog;
use backend\widgets\Menu;
use common\models\TimelineEvent;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\log\Logger;
use yii\widgets\Breadcrumbs;

$bundle = BackendAsset::register($this);
?>
<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper">
    <!-- header logo: style can be found in header.less -->
    <header class="main-header">
        <a href="<?php echo Yii::$app->urlManagerFrontend->createAbsoluteUrl('/') ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <?php echo Yii::$app->name ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?php echo Yii::t('backend', 'Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                 class="user-image">
                            <span><?php echo Yii::$app->user->identity->username ?> <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header light-blue">
                                <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                                     class="img-circle" alt="User Image"/>
                                <p>
                                    <?php echo Yii::$app->user->identity->username ?>
                                    <small>
                                        <?php echo Yii::t('backend', 'Member since {0, date, short}', Yii::$app->user->identity->created_at) ?>
                                    </small>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?php echo Html::a(Yii::t('backend', 'Profile'), ['/sign-in/profile'], ['class' => 'btn btn-default btn-flat']) ?>
                                </div>
                                <div class="pull-left">
                                    <?php echo Html::a(Yii::t('backend', 'Account'), ['/sign-in/account'], ['class' => 'btn btn-default btn-flat']) ?>
                                </div>
                                <div class="pull-right">
                                    <?php echo Html::a(Yii::t('backend', 'Logout'), ['/sign-in/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?php echo Html::a('<i class="fa fa-cogs"></i>', ['/site/settings']) ?>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar($this->assetManager->getAssetUrl($bundle, 'img/anonymous.jpg')) ?>"
                         class="img-circle"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::t('backend', 'Hello, {username}', ['username' => Yii::$app->user->identity->getPublicIdentity()]) ?></p>
                    <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                        <i class="fa fa-circle text-success"></i>
                        <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                    </a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php echo Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items' => [
                    [
                        'label' => Yii::t('backend', 'Main'),
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Новости',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/news/index'],
                        'badge' => \common\models\News::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Дилерам',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/dealer-category/index'],
                        'badge' => \common\models\DealerCategory::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'О нас',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/about-category/index'],
                        'badge' => \common\models\AboutCategory::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Магазин',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Каталог',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/product-category/index'],
                        'badge' => \common\models\ProductCategory::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Заказы',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/order/index'],
                        'badge' => \common\models\Order::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Почта для заказов',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/email/update', 'id' => 1],
                    ],
                    [
                        'label' => 'Контакты',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Администрация',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/contact-admin/index'],
                        'badge' => \common\models\ContactAdmin::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Регионы',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/contact-region-name/index'],
                    ],
                    [
                        'label' => 'Реквизиты',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/requisites/index'],
                    ],
                    [
                        'label' => 'Другое',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Информация',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/articles/index'],
                        'badge' => \common\models\Articles::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Где купить',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/region/index'],
                        'badge' => \common\models\Store::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Схема проезда',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/map/update', 'id' => 1],
                    ],
                    [
                        'label' => 'SEO',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Страницы',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/seo/index'],
                        'badge' => \common\models\Seo::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                    [
                        'label' => 'Ссылки',
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => 'Соц.сети',
                        'icon' => '<i class="fa fa-angle-double-right"></i>',
                        'url' => ['/social-network/index'],
                        'badge' => \common\models\SocialNetwork::find()->count(),
                        'badgeBgClass' => 'label-success',
                    ],
                ]
            ]) ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $this->title ?>
                <?php if (isset($this->params['subtitle'])): ?>
                    <small><?php echo $this->params['subtitle'] ?></small>
                <?php endif; ?>
            </h1>

            <?php echo Breadcrumbs::widget([
                'tag' => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if (Yii::$app->session->hasFlash('alert')): ?>
                <?php echo Alert::widget([
                    'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ]) ?>
            <?php endif; ?>
            <?php echo $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<?php $this->endContent(); ?>
