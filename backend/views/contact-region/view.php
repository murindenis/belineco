<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ContactRegion */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Региональные представители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-region-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этого представителя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'position',
                'label' => 'Должность'
            ],
            [
                'attribute' => 'name',
                'label' => 'ФИО'
            ],
            [
                'attribute' => 'phone_mob',
                'label' => 'Мобильный'
            ],
            [
                'attribute' => 'phone_home',
                'label' => 'Телефон'
            ],
            [
                'attribute' => 'fax',
                'label' => 'Факс'
            ],
            'email',
            'skype',
            'created_at:datetime',
        ],
    ]) ?>

</div>
