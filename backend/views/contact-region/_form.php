<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ContactRegion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-region-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contact_region_name_id')->hiddenInput(['value' => Yii::$app->request->get('crnid')])->label(false) ?>

    <?= $form->field($model, 'position')->textInput()->label('Должность') ?>

    <?= $form->field($model, 'name')->textInput()->label('ФИО') ?>

    <?= $form->field($model, 'phone_mob')->textInput()->label('Мобильный') ?>

    <?= $form->field($model, 'phone_home')->textInput()->label('Телефон') ?>

    <?= $form->field($model, 'fax')->textInput()->label('Факс') ?>

    <?= $form->field($model, 'email')->textInput()->label('Email') ?>

    <?= $form->field($model, 'skype')->textInput()->label('Skype') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
