<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ContactRegionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Региональные представители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-region-index">

    <p>
        <?= Html::a('Добавить представителя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'position',
            'name',
            'phone_mob',
            'phone_home',
            'fax',
            'email:email',
            'skype',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
