<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactRegion */

$this->title = 'Редактировать представителя';
$this->params['breadcrumbs'][] = ['label' => 'Региональные представители', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="contact-region-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
