<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ContactRegion */

$this->title = 'ДОбавить представителя';
$this->params['breadcrumbs'][] = ['label' => 'Региональные представители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-region-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
