<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Map */

$this->title = 'Добавить карту';
$this->params['breadcrumbs'][] = ['label' => 'Карта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
