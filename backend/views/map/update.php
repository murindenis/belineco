<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Map */

$this->title = 'Редактировать схему проезда';
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="map-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
