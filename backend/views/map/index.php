<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Схема проезда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-index">

    <p>
        <?= Html::a('Добавить карту', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'base_url:url',
            'path',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
