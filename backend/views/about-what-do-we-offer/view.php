<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AboutWhatDoWeOffer */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'About What Do We Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-what-do-we-offer-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'about_category_id',
            'title',
            'introtext:ntext',
            'text:ntext',
            'path',
            'base_url:url',
            'created_at:datetime',
        ],
    ]) ?>

</div>
