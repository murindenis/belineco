<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutWhatDoWeOffer */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Yfib ghtlkj;tybz', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-what-do-we-offer-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
