<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AboutWhatDoWeOfferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'About What Do We Offers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-what-do-we-offer-index">

    <p>
        <?= Html::a('Create About What Do We Offer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'about_category_id',
            'title',
            'introtext:ntext',
            'text:ntext',
            //'path',
            //'base_url:url',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
