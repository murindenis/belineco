<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\AboutWhatDoWeOfferSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-what-do-we-offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'about_category_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'introtext') ?>

    <?= $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'path') ?>

    <?php // echo $form->field($model, 'base_url') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
