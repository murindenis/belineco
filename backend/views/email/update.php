<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Email */

$this->title = 'Редактировать Email';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
