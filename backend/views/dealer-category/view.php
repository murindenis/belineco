<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DealerCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Дилерам', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-category-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы хотите удалить этот раздел?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Документы</h3>
    <p>
        <?= Html::a('Добавить документ', ['/dealer-file/create', 'dcid' => Yii::$app->request->get('id')], ['class' => 'btn btn-success']) ?>
    </p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'type',
            'path',
            'base_url',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'dealer-file'
            ],
        ],
    ]); ?>

</div>
