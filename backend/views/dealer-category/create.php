<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DealerCategory */

$this->title = 'Добавить раздел';
$this->params['breadcrumbs'][] = ['label' => 'Дилерам', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
