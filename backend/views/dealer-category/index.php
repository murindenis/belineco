<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DealerCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Дилерам';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-category-index">

    <p>
        <?= Html::a('Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
