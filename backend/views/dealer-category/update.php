<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DealerCategory */

$this->title = 'Редактировать раздел';
$this->params['breadcrumbs'][] = ['label' => 'Дилерам', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать раздел';
?>
<div class="dealer-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
