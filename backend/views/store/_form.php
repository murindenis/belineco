<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Store */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'store_city_name_id')->hiddenInput(['value' => Yii::$app->request->get('scnid')])->label(false) ?>

    <?= $form->field($model, 'name')->textInput()->label('Название') ?>

    <?= $form->field($model, 'address')->textInput()->label('Адрес') ?>

    <?= $form->field($model, 'phone_mob')->textInput()->label('Мобильный') ?>

    <?= $form->field($model, 'phone_home')->textInput()->label('Телефон') ?>

    <?= $form->field($model, 'fax')->textInput()->label('Факс') ?>

    <?= $form->field($model, 'email')->textInput()->label('Email') ?>

    <?= $form->field($model, 'skype')->textInput()->label('Skype') ?>

    <?= $form->field($model, 'site')->textInput()->label('Сайт') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
