<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AboutPhoto */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Фото', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-photo-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
