<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutPhoto */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Фото', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="about-photo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
