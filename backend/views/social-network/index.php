<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SocialNetworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Соц.сети';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-network-index">

    <p>
        <?= Html::a('Добавить соц.сеть', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'link',
            'path',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
