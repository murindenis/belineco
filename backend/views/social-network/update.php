<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SocialNetwork */

$this->title = 'Редактировать Соц.сеть';
$this->params['breadcrumbs'][] = ['label' => 'Соц.сети', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="social-network-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
