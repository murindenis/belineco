<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SocialNetwork */

$this->title = 'Добавить соц.сеть';
$this->params['breadcrumbs'][] = ['label' => 'Соц.сети', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-network-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
