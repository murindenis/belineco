<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StoreCityName */

$this->title = 'Редактировать город';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="store-city-name-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
