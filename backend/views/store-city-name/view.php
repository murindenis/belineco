<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StoreCityName */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-city-name-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите этот город со всеми магазинами?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region_id',
            'name',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Магазины</h3>
    <p>
        <?= Html::a('Добавить магазин', ['/store/create', 'scnid' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'name',
                'label' => 'Название',
            ],
            [
                'attribute' => 'address',
                'label' => 'Адрес',
            ],
            [
                'attribute' => 'phone_mob',
                'label' => 'Мобильный',
            ],
            [
                'attribute' => 'phone_home',
                'label' => 'Телефон',
            ],
            [
                'attribute' => 'fax',
                'label' => 'Факс',
            ],
            [
                'attribute' => 'email',
                'label' => 'Email',
            ],
            [
                'attribute' => 'skype',
                'label' => 'Skype',
            ],
            [
                'attribute' => 'site',
                'label' => 'Сайт',
            ],

            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'store'
            ],
        ],
    ]); ?>

</div>
