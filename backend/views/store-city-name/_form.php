<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StoreCityName */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="store-city-name-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->hiddenInput(['value' => Yii::$app->request->get('rid')])->label(false) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
