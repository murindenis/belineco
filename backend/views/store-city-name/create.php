<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StoreCityName */

$this->title = 'Добавить город';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-city-name-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
