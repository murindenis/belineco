<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AboutOurPurposeAndMission;

/**
 * AboutOurPurposeAndMissionSearch represents the model behind the search form of `common\models\AboutOurPurposeAndMission`.
 */
class AboutOurPurposeAndMissionSearch extends AboutOurPurposeAndMission
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'about_category_id'], 'integer'],
            [['title', 'introtext', 'text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AboutOurPurposeAndMission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'about_category_id' => $this->about_category_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
