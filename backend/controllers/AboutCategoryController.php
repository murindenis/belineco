<?php

namespace backend\controllers;

use backend\models\search\AboutOurPurposeAndMissionSearch;
use backend\models\search\AboutPhotoSearch;
use backend\models\search\AboutProductSearch;
use backend\models\search\AboutWhatDoWeOfferSearch;
use backend\models\search\AboutWhoAreWeSearch;
use function GuzzleHttp\normalize_header_keys;
use Yii;
use common\models\AboutCategory;
use backend\models\search\AboutCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AboutCategoryController implements the CRUD actions for AboutCategory model.
 */
class AboutCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AboutCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AboutCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        switch ($id) {
            case '1':
                $searchModel = new AboutWhoAreWeSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $columns = [
                    'id',
                    'title',
                    'introtext',
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'about-who-are-we',
                        'template'=>'{view}{update}'
                    ],
                ];
                break;
            case '2':
                $searchModel = new AboutWhatDoWeOfferSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $columns = [
                    'id',
                    'title',
                    'introtext',
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'about-what-do-we-offer',
                        'template'=>'{view}{update}'
                    ],
                ];
                break;
            case '3':
                $searchModel = new AboutProductSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $columns = [
                    'id',
                    'title',
                    'path',
                    'base_url',
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'about-product',
                        'template'=>'{view}{update}'
                    ],
                ];
                break;
            case '4':
                $searchModel = new AboutOurPurposeAndMissionSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $columns = [
                    'id',
                    'title',
                    'introtext',
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'about-our-purpose-and-mission',
                        'template'=>'{view}{update}'
                    ],
                ];
                break;
            case '5':
                $searchModel = new AboutPhotoSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $columns = [
                    'id',
                    'title',
                    'path',
                    'base_url',
                    'created_at:datetime',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'about-photo',
                    ],
                ];
                break;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel'  => $searchModel  ?? [],
            'dataProvider' => $dataProvider ?? [],
            'columns'      => $columns,
        ]);
    }

    /**
     * Creates a new AboutCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AboutCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AboutCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AboutCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AboutCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AboutCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AboutCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
