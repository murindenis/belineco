<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],

        // Sitemap
        ['pattern' => 'sitemap', 'route' => 'sitemap/default/index', 'suffix' => '.xml'],

        ['pattern' => 'category/<slug>', 'route' => 'catalog/index'],
        ['pattern' => 'product/<slug>', 'route' => 'catalog/page'],
        ['pattern' => 'news', 'route' => 'site/news'],
        ['pattern' => 'news/<slug>', 'route' => 'site/news-page'],
        ['pattern' => 'articles', 'route' => 'site/articles'],
        ['pattern' => 'articles/<slug>', 'route' => 'site/articles-page'],
        ['pattern' => 'abouts', 'route' => 'site/abouts'],
        ['pattern' => 'dealers', 'route' => 'site/dealers'],
        ['pattern' => 'contacts', 'route' => 'site/contacts'],
        ['pattern' => 'services', 'route' => 'site/services'],
        ['pattern' => 'where-buy', 'route' => 'site/where-buy'],
        ['pattern' => 'maps', 'route' => 'site/maps'],
    ]
];
