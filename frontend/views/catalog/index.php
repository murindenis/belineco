<?php
$title = !empty($category->seo_title)
    ? $category->seo_title
    : '';

$keywords = !empty($category->seo_keywords)
    ? $category->seo_keywords
    : '';

$description = !empty($category->seo_description)
    ? $category->seo_description
    : '';

$this->title = $title;
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://belineco.com/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= $category->name ?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($category->name, ['/catalog/index', 'slug' => $category->slug], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="/img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<!--main-->
<main class="main">
    <div class="content__wrapper about__company__page">
        <div class="content__min__width">
            <div class="tabs__wrapper">
                <h2 class="tabs__title">Категории</h2>
                <?php foreach ($brands as $key=>$brand) { ?>
                    <div class="tabs__item <?= $key == false ? 'active' :''?>"><a href="#brand_<?= $brand->id ?>"><?= $brand->name ?></a></div>
                <? } ?>
            </div>
        </div>
        <div class="content__max__width">
            <svg class="icon icon__fixed">
                <use xlink:href="/img/sprite.svg#icon-menu-fixed"></use>
            </svg>
            <h1 class="title__from__mobile">
                <?= $category->name ?></h1>
            <?php foreach ($brands as $brand) { ?>
            <section class="section__item" id="brand_<?= $brand->id ?>">
                <?php foreach ($brand->products as $product) { ?>
                <div class="product__in__catalog">
                    <div class="photo__product"
                         style="background: url(<?= $product->getImgPath() ?>) no-repeat;
                                 background-position: center;
                                 background-size: contain">

                    </div>
                    <div class="info__about__product">
                        <div class="top__info__about__product">
                            <div class="title__product">
                                <h2 class="name__firm"><?= $product->getBrandNameById($product->product_brand_id) ?></h2>
                                <h2 class="title__product"><?= $product->name ?></h2>
                            </div>
                            <p><?= $product->introtext ?></p>
                            <?= \yii\helpers\Html::a('<span>Подробнее</span>
                                <svg class="icon">
                                    <use xlink:href="/img/sprite.svg#icon-arrow"></use>
                                </svg>', ['/catalog/page', 'slug' => $product->slug])?>
                        </div>
                        <div class="footer__product__in__catalog">
                            <?php if ($product->option_amount) { ?>
                                <div class="footer__product__item"><span>Объем: </span><span><?= $product->option_amount?>мл</span></div>
                            <? } ?>
                            <?php /*if ($product->option_calibr) { */?><!--
                                <div class="footer__product__item"><span>Калибр: </span><span><?/*= $product->option_calibr*/?></span></div>
                            --><?/* } */?>
                            <?php if ($product->option_type) { ?>
                                <div class="footer__product__item"><span>Тип: </span><span><?= $product->option_type?></span></div>
                            <? } ?>
                            <?php if ($product->option_color) { ?>
                                <div class="footer__product__item"><span>Цвет: </span><span><?= $product->option_color?></span></div>
                            <? } ?>
                        </div>
                    </div>
                </div>
                <? } ?>
            </section>
            <? } ?>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>