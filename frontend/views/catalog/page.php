<?php
$title = !empty($product->seo_title)
    ? $product->seo_title
    : '';

$keywords = !empty($product->seo_keywords)
    ? $product->seo_keywords
    : '';

$description = !empty($product->seo_description)
    ? $product->seo_description
    : '';

$this->title = $title;
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://belineco.com/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<!--header-->
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= $product->name?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($product->productCategory->name.'>', ['/catalog/index', 'slug' => $product->productCategory->slug], ['class' => 'breadcrumbs__item'])?>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($product->name, ['/catalog/page', 'slug' => $product->slug], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="/img/sprite.svg#icon-menu"> </use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<!--main-->
<main class="main">
    <div class="content__wrapper">
        <div class="content__min__width">
            <div class="product__img"><img src="<?= $product->getImgPath()?>"></div>
        </div>
        <div class="content__max__width">
            <h1 class="title__from__mobile">
                <?= $product->name?>
            </h1>
            <div class="description__table with__margin">
            <?php if ($product->characteristic) { ?>
                <div class="header__description">
                    <p>Характеристика</p>
                </div>
                <div class="description__item">
                    <div class="bottom__description content__character"><?= $product->characteristic ?></div>
                </div>
            <? } ?>
            </div>
            <?php $i = 1;?>
            <?php foreach ($productOptions as $productOption) { ?>
            <div class="description__table with__margin">
                <div class="header__description">
                    <p><?= $productOption->name ?></p>
                </div>
                <div class="description__item">
                    <div class="bottom__description content__character"><?= $productOption->value ?></div>
                </div>
            </div>
            <? $i++;} ?>

            <div class="form">
                <div class="header__form">
                    <h3>Хотите заказать?</h3>
                </div>
                <form class="main__form" id="orderForm">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
                    <input type="hidden" name="product_id" value="<?= $product->id?>">
                    <div class="form__item">
                        <label for="name">Имя</label>
                        <input type="text" name="firstname" id="name">
                    </div>
                    <div class="form__item">
                        <label for="surname">Фамилия</label>
                        <input type="text" name="lastname" id="surname">
                    </div>
                    <div class="form__item">
                        <label for="phone">Телефон</label>
                        <input type="text" name="phone" id="phone">
                    </div>
                    <div class="form__item">
                        <label for="country">Страна</label>
                        <input type="text" name="country" id="country">
                    </div>
                    <div class="form__item">
                        <label for="firm">Фирма</label>
                        <input type="text" name="firm" id="firm">
                    </div>
                    <div class="form__item">
                        <label for="position">Должность</label>
                        <input type="text" name="position" id="position">
                    </div>
                    <div class="form__item">
                        <label for="email">E-mail</label>
                        <input type="text" name="email" id="email">
                    </div>
                    <div class="form__item">
                        <button class="newOrderBtn">Отправить</button>
                    </div>
                </form>
                <div class="formMsg" style="padding:0 30px 10px; background: #fff;"></div>
            </div>
            <div class="partners">
                <?php foreach ($product->productCategory->productBrands as $brand) {?>
                <div class="partners__item"><img src="<?= $brand->getImgPath()?>"></div>
                <? } ?>
            </div>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
