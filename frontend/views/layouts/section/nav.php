<?php
$productCategories = \common\models\ProductCategory::find()->orderBy(['sort' => SORT_ASC])->all();
?>
<nav class="nav">
    <div class="language">
        <!-- <select>
            <option>EN</option>
            <option>RU</option>
        </select> -->
        <svg class="icon icon__close">
            <use xlink:href="/img/sprite.svg#icon-close"></use>
        </svg>
    </div>
    <div class="menu">
        <ul>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/news'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'news' ? 'active' : ''?>">Новости</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/abouts'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'abouts' ? 'active' : ''?>">О нас</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/services'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'services' ? 'active' : ''?>">Услуги</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/dealers'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'dealers' ? 'active' : ''?>">Дилерам</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="product">
        <ul>
            <?php foreach ($productCategories as $productCategory) {?>
            <li class="<?= Yii::$app->request->get('slug') == $productCategory->slug ? 'active' : ''?>">
                <?= \yii\helpers\Html::a('<span>'.$productCategory->name.'</span>',
                    ['/catalog/index' , 'slug' => $productCategory->slug])?>
            </li>
            <? } ?>
        </ul>
    </div>
    <div class="menu__more">
        <ul>
            <!--<li><a href="#"><span>Информация</span></a></li>-->
            <!--<li><a href="#"><span>Торговые марки</span></a></li>-->
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/articles'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'articles' ? 'active' : ''?>">Информация</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/where-buy'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'where-buy' ? 'active' : ''?>">Где купить</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/contacts'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'contacts' ? 'active' : ''?>">Контакты</span>
                </a>
            </li>
            <li>
                <a href="<?= \yii\helpers\Url::toRoute(['/site/maps'])?>">
                    <span class="<?= Yii::$app->controller->action->id == 'maps' ? 'active' : ''?>">Схема проезда</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="footer__nav">
        <div class="tel__link"><a href="tel:+375162973022">+375 162 97-30-22</a></div>
        <div class="mail__link"><a href="mailto:office@belineco.com">office@belineco.com</a></div>
        <div class="social">
            <?php
                $socialModel = \common\models\SocialNetwork::find()->all();
            ?>
            <?php foreach ($socialModel as $social) {?>
            <div class="social__item">
                <a href="<?= $social->link?>">
                    <svg class="icon">
                        <use xlink:href="/img/sprite.svg#<?= $social->path?>"></use>
                    </svg>
                </a>
            </div>
            <? } ?>
            <!--<div class="social__item">
                <a href="#">
                <svg class="icon">
                    <use xlink:href="/img/sprite.svg#icon-facebook"></use>
                </svg>
                </a>
            </div>
            <div class="social__item">
                <a href="#">
                <svg class="icon">
                    <use xlink:href="/img/sprite.svg#icon-twitter"></use>
                </svg>
                </a>
            </div>
            <div class="social__item">
                <a href="#">
                <svg class="icon">
                    <use xlink:href="/img/sprite.svg#icon-vk"></use>
                </svg>
                </a>
            </div>-->
        </div>
    </div>
</nav>