<header>
    <div class="logo"><a href="/"><img src="/img/logo.svg"/></a></div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <div class="search__block bg__inherit">
        <svg class="icon icon_search">
            <use xlink:href="/img/sprite.svg#icon-search"></use>
        </svg>
    </div>
</header>