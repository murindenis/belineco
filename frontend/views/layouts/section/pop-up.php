<div class="pop-up">
    <svg class="icon icon_close">
        <use xlink:href="/img/sprite.svg#icon-close"></use>
    </svg>
    <div class="pop-up-wrapper">
        <form class="search__form" action="<?= \yii\helpers\Url::toRoute(['site/search'])?>">
            <input class="" id="search" type="search" name="search" placeholder="Поиск">
            <svg class="icon icon_search">
                <use xlink:href="/img/sprite.svg#icon-search"></use>
            </svg>
        </form>
    </div>
</div>