<?php
if (Yii::$app->controller->action->id == 'index') {
    $page = '/';
} else {
    $page = Yii::$app->controller->action->id;
}

$seo = \common\models\Seo::find()->where(['page' => $page])->one();

$title = !empty($seo->title)
    ? $seo->title
    : '';

$keywords = !empty($seo->keywords)
    ? $seo->keywords
    : '';

$description = !empty($seo->description)
    ? $seo->description
    : '';

$this->title = $title;
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://belineco.com/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'    => 'DC.title',
    'content' => $title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
