<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

/* @var $content string */

$this->beginContent('@frontend/views/layouts/belineco.php');

echo $content;
require_once(Yii::getAlias('@frontend').'/views/layouts/section/pop-up.php');
$this->endContent();
