<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/header.php');
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<!--main-->
<main class="main">
    <div class="slider-for">
        <?php foreach ($products as $product) { ?>
        <div class="slider__item">
            <div class="slider__wrapper">
                <div class="slider__content">
                    <p>Продукт</p>
                    <h2><?= $product->name?></h2><!--<span><?/*= $product->introtext*/?> </span>-->
                    <div class="link__more">
                        <?= \yii\helpers\Html::a('Подробнее', ['catalog/page', 'slug' => $product->slug])?>
                    </div>
                </div>
                <div class="slider__img" style="background: url(<?= $product->getImgPath() ;?>) no-repeat;   background-size: contain; background-position: center;">
                </div>
            </div>
        </div>
        <? } ?>
    </div>
    <div class="slider-nav">
        <?php $i = 1;?>
        <?php foreach ($products as $product) { ?>
        <div class="slider__item__nav">
            <div class="nav__slide">
                <div class="nav__tro">
                    <p class="number__slide"><?= $i?></p>
                    <p class="title__slide"><?= $product->name?></p>
                </div>
            </div>
        </div>
        <? $i++;} ?>
    </div>
    <div class="slider-progress">
        <div class="progress"></div>
    </div>
</main>
<!--footer-->