<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<!--header-->
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Дилерам', ['site/dealers'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="/img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="content__wrapper about__company__page">
        <div class="content__min__width">
            <div class="tabs__wrapper">
                <h2 class="tabs__title">Разделы</h2>
                <?php foreach ($dealerCategories as $key=>$dealerCategory) { ?>
                <div class="tabs__item <?= !$key ? 'active' : ''?>">
                    <a href="#dealer_<?= $dealerCategory->id?>"><?= $dealerCategory->name?></a>
                </div>
                <? } ?>
            </div>
        </div>
        <div class="content__max__width">
            <svg class="icon icon__fixed">
                <use xlink:href="img/sprite.svg#icon-menu-fixed"></use>
            </svg>
            <h1 class="title__from__mobile">Дилерам</h1>
            <?php foreach ($dealerCategories as $dealerCategory) { ?>
            <section class="section__item" id="dealer_<?= $dealerCategory->id?>">
                <h2 class="title__document"><?= $dealerCategory->name?></h2>
                <div class="document__list">
                    <?php foreach ($dealerCategory->files as $file) { ?>
                    <?php $type = substr($file->type, strpos($file->type, '/'));?>
                    <div class="document__item">
                        <?php
                        if (!file_exists(\Yii::getAlias('@frontend').'/web/img/icon-file'.$type.'.svg')) {
                            $typeName = 'default';
                        } else {
                            $typeName = substr($file->type, strpos($file->type, '/'));
                        }
                        ?>

                        <img class="icon" src="/img/icon-file/<?= $typeName?>.svg">

                        <a href="<?= $file->getFilePath()?>" download><?= $file->name?></a>
                    </div>
                    <? } ?>
                </div>
            </section>
            <? } ?>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
