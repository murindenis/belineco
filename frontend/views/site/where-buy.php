<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= $seo->h1?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Где купить', ['site/where-buy'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="contact__page">
        <h1 class="title__from__mobile">
            Где купить</h1>
        <iframe width="1440" height="480" src="https://www.google.com/maps/d/embed?mid=1FNkEz3Sf56qrQ6x5oKVF_HL_2-8"></iframe>
        <?php foreach ($regions as $region) {?>
            <?php foreach ($region->storeCityNames as $city) {?>
                <?php if (!empty($city->stores)) {?>
                    <h2 class="contact__title">
                        <div class="container"><p><?= $city->name?></p></div>
                    </h2>
                    <?php foreach ($city->stores as $store) {?>
                        <div class="contact__background">
                            <div class="container">
                                <div class="contact__item">
                                    <!--<div class="position">
                                        <p><?//= $store->position?></p>
                                    </div>-->
                                    <div class="contact__info">
                                        <div class="name"><?= $store->name?></div>
                                        <?php if ($store->address) {?>
                                            <div class="title__contact__item">Адрес</div>
                                            <div class="info__contact__item"><?= $store->address?></div>
                                        <? } ?>
                                        <?php if ($store->phone_mob) {?>
                                            <div class="title__contact__item">Мобильный</div>
                                            <div class="info__contact__item"><?= $store->phone_mob?></div>
                                        <? } ?>
                                        <?php if ($store->phone_home) {?>
                                            <div class="title__contact__item">Телефон</div>
                                            <div class="info__contact__item"><?= $store->phone_home?></div>
                                        <? } ?>
                                        <?php if ($store->fax) {?>
                                            <div class="title__contact__item">Факс</div>
                                            <div class="info__contact__item"><?= $store->fax?></div>
                                        <? } ?>
                                        <?php if ($store->email) {?>
                                            <div class="title__contact__item">Email</div>
                                            <div class="info__contact__item">
                                                <a href="mailto:<?= $store->email?>"><?= $store->email?></a>
                                            </div>
                                        <? } ?>
                                        <?php if ($store->skype) {?>
                                            <div class="title__contact__item">Skype</div>
                                            <div class="info__contact__item"><?= $store->skype?></div>
                                        <? } ?>
                                        <?php if ($store->site) {?>
                                            <div class="title__contact__item">Сайт</div>
                                            <div class="info__contact__item"><?= $store->site?></div>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                <? } ?>
            <? } ?>
        <? } ?>
        <?php
        require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer-box.php');
        ?>
    </div>
</main>
