<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('О нас', ['site/news'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
        <?php
        require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
        ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<!--main-->
<main class="main">
    <div class="content__wrapper about__company__page">
        <div class="content__min__width">
            <div class="tabs__wrapper">
                <h2 class="tabs__title">Разделы</h2>
                <?php foreach ($aboutCategories as $key=>$aboutCategory) { ?>
                    <div class="tabs__item <?= !$key ? 'active' : ''?>">
                        <a href="#about_<?= $aboutCategory->id?>"><?= $aboutCategory->name?></a>
                    </div>
                <? } ?>
            </div>
        </div>
        <div class="content__max__width">
            <svg class="icon icon__fixed">
                <use xlink:href="img/sprite.svg#icon-menu-fixed"></use>
            </svg>
            <h1 class="title__from__mobile"></h1>
            <section class="section__item" id="about_1">
                <div class="wrapper__about_company">
					<div class="section__title">
						<h2 class="russian"><?= $block1->title?></h2>
					</div>
                    <div class="content__about__us__flex section__wrapper reverse__block">
                        <div class="content__about__us__flex__img">
                            <img src="<?= $block1->getImgPath()?>">
                        </div>
                        <div class="content__about__us__flex__text">
                            <p>
                                <?= $block1->introtext?>
                            </p>
                        </div>
                    </div>
                    <div class="content__about__us__normal">
                        <p><?= $block1->text?></p>
                    </div>
                </div>
            </section>
            <section class="section__item" id="about_2">
                <div class="wrapper__about_company offers">
					<div class="section__title">
						<h2 class="russian"><?= $block2->title?></h2>
					</div>
                    <div class="content__about__us__flex section__wrapper">
                        <div class="content__about__us__flex__text">
                            <p><?= $block2->introtext?></p>
                        </div>
                        <div class="content__about__us__flex__img">
                            <div class="company__list">
                                <img src="<?= $block2->getImgPath()?>">
                            </div>
                        </div>
                    </div>
                    <div class="content__about__us__normal">
                        <p><?= $block2->text?></p>
                    </div>
                </div>
            </section>
            <section class="section__item" id="about_3">
                <div class="product__list">
                    <div class="product__list__item"><img src="<?= $block3->getImgPath()?>"></div>
                </div>
            </section>
            <section class="section__item" id="about_4">
                <div class="mission__wrapper">
                    <div class="mission__top">
                        <div class="section__title">
                            <h2 class="russian"><?= $block4->title?></h2>
                        </div>
                        <p><?= $block4->introtext?></p>
                    </div>
                    <div class="mission__bottom">
                        <p><?= $block4->text?></p>
                    </div>
                </div>
            </section>
            <section class="section__item" id="about_5">
                <div class="photo__about__us">
                    <?php foreach ($photos as $photo) {?>
                    <div class="photo__item"><img src="<?= $photo->getImgPath()?>"></div>
                    <? } ?>
                </div>
            </section>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
