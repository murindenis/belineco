<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Услуги', ['site/services'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="container">
        <div class="services__page">
            <h1 class="title__from__mobile">Услуги</h1>
            <div class="services__content">
                <h2>МЫ ПРЕДЛАГАЕМ:</h2>
                <ol>
                    <li>Монтажную пену, клей-пену и очиститель монтажной пены в Аэрозольной упаковке под Вашей торговой марков.</li>
                    <li>Жестяные аэрозольные баллоны диаметром 65мм. </li>
                </ol>
                <div class="services__list">
                    <div class="services__item">
                        <!--<svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-close"></use>
                        </svg>-->
                        <div style="width:60px; background: url('/img/services/upakovka.png') no-repeat; background-size: contain; background-position-x: center"></div>
                        <div class="services__item__content">
                            <h3>1. Упаковка</h3>
                            <p>Размер баллона, мм :</p>
                            <ul>
                                <li>65 x 195 (300 мл)</li>
                                <li>65 х 300 (1000 мл)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="services__item">
                        <!--<svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-close"></use>
                        </svg>-->
                        <div style="width:60px; background: url('/img/services/etiketka.png') no-repeat; background-size: contain; background-position-x: center"></div>
                        <div class="services__item__content">
                            <h3>2. Этикетка</h3>
                            <p>Материал :</p>
                            <ul>
                                <li>литография</li>
                                <li>бумага</li>
                            </ul>
                        </div>
                    </div>
                    <div class="services__item">
                        <!--<svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-close"></use>
                        </svg>-->
                        <div style="width:60px; background: url('/img/services/obem.png') no-repeat; background-size: contain; background-position-x: center"></div>
                        <div class="services__item__content">
                            <h3>3. Объем</h3>
                            <p>Минимальная партия 1-го наименования:</p>
                            <ul>
                                <li>5 000шт. - бумага</li>
                                <li>15 000шт. -литография</li>
                            </ul>
                        </div>
                    </div>
                    <div class="services__item">
                        <!--<svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-close"></use>
                        </svg>-->
                        <div style="width:60px; background: url('/img/services/sroki.svg') no-repeat; background-size: contain; background-position-x: center"></div>
                        <div class="services__item__content">
                            <h3>4. Сроки</h3>
                            <p>Срок изготовления первой партии до 3-х недель.</p>
                        </div>
                    </div>
                    <div class="services__item">
                        <!--<svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-close"></use>
                        </svg>-->
                        <div style="width:60px; background: url('/img/services/cena.svg') no-repeat; background-size: contain; background-position-x: center"></div>
                        <div class="services__item__content">
                            <h3>5. Цена</h3>
                            <p>В зависимости от объема заказа.</p>
                        </div>
                    </div>
                </div>
                <h2>НАШИ ПРЕИМУЩЕСТВА:</h2>
                <div class="advantages__list">
                    <div class="advantages__item">
                        <div class="advantages__item__img"><img src="/img/services/service1.png"></div>
                        <div class="advantages__item__content">
                            <p>Исполнение заказа по индивидуальному дизайну, баллоны различного типоразмера</p>
                        </div>
                    </div>
                    <div class="advantages__item">
                        <div class="advantages__item__img"><img src="/img/services/service2.png"></div>
                        <div class="advantages__item__content">
                            <p>Применение внутренней и наружной лакировки сварочного шва в целях предохранения от коррозии</p>
                        </div>
                    </div>
                    <div class="advantages__item">
                        <div class="advantages__item__img"><img src="/img/services/service3.png"></div>
                        <div class="advantages__item__content">
                            <p>Производственные линии, изготовленные по заказу мировыми лидерами отрасли: Soudronic, lanico. Минимальный срок переналадки</p>
                        </div>
                    </div>
                    <div class="advantages__item">
                        <div class="advantages__item__img"><img src="/img/services/service4.png"></div>
                        <div class="advantages__item__content">
                            <p>Использование сырья от ведущих европейских производителей</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
