<?php
$title = !empty($news->seo_title)
    ? $news->seo_title
    : '';

$keywords = !empty($news->seo_keywords)
    ? $news->seo_keywords
    : '';

$description = !empty($news->seo_description)
    ? $news->seo_description
    : '';

$this->title = $title;
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://belineco.com/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'     => 'DC:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= $news->title?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <?= \yii\helpers\Html::a('Новости', ['site/news'], ['class' => 'breadcrumbs__item'])?>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($news->title, ['site/news-page', 'slug' => $news->slug], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="/img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="news__wrapper">
        <div class="news__item__page">
            <h1 class="title__from__mobile">
                <?= $news->title?>
            </h1>
            <div class="news__description">
                <?= $news->description?>
            </div>
            <h1 class="title__more__news__list">Новости</h1>
            <div class="more__news__list">
                <?php foreach ($newsAll as $news) {
                    echo \yii\helpers\Html::a('<div class="news__img">
                        <img src="'.$news->getImgPath().'">
                    </div>
                    <h2>'.$news->title.'</h2>
                    <span>'.$news->introtext.'</span>', ['site/news-page', 'slug' => $news->slug], ['class' => 'little__news__item']);
                } ?>
            </div>
            <div class="button__news">
                <?= \yii\helpers\Html::a('Перейти к новостям', ['site/news'], ['class' => 'more__news__button'])?>
            </div>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
