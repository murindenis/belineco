<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<!--header-->
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Новости', ['site/news'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <h1 class="title__from__mobile">Новости</h1>
    <div class="news__wrapper">
        <div class="news__content">
            <?= \yii\helpers\Html::a('<div class="news__img"><img src="'.$lastNews->getImgPath().'"></div>
                <h2>'.$lastNews->title.'</h2>
                <span>'.$lastNews->introtext.'</span>', ['site/news-page', 'slug' => $lastNews->slug] , ['class' => 'big__news'])?>
            <div class="little__news__list">
                <?php foreach ($news as $key=>$item) {?>
                    <?= \yii\helpers\Html::a('<div class="news__img"><img src="'.$item->getImgPath().'"></div>
                    <h2>'.$item->title.'</h2>
                    <span>'.$item->introtext.'</span>', ['site/news-page', 'slug' => $item->slug], ['class' => 'little__news__item'])?>
                <? } ?>
            </div>
        </div>
        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
        <!--<div class="pagination"><a class="pagination__item" href="#">Первая</a>
            <ul>
                <li><a href="#">
                        <svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-pagination-prev"></use>
                        </svg></a></li>
                <li class="active"><a href="#">10</a></li>
                <li><a href="#">11</a></li>
                <li><a href="#">..</a></li>
                <li><a href="#">25</a></li>
                <li><a href="#">26</a></li>
                <li><a href="#">
                        <svg class="icon">
                            <use xlink:href="img/sprite.svg#icon-pagination-next"></use>
                        </svg></a></li>
            </ul><a class="pagination__item" href="#">Последняя</a>
        </div>-->
        <?php
        require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer-box.php');
        ?>
    </div>
</main>
