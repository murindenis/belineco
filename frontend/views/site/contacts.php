<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Контакты', ['site/contacts'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="contact__page">
        <h1 class="title__from__mobile">
            КОНТАКТЫ</h1>
        <h2 class="contact__title">
            <div class="container"><p>Администрация</p></div>
        </h2>
        <?php foreach ($contactAdminModel as $contactAdmin) {?>
        <div class="contact__background">
            <div class="container">
                <div class="contact__item">
                    <div class="position">
                        <p><?= $contactAdmin->position?></p>
                    </div>
                    <div class="contact__info">
                        <div class="name"><?= $contactAdmin->name?></div>
                        <?php if ($contactAdmin->phone_mob) {?>
                            <div class="title__contact__item">Мобильный</div>
                            <div class="info__contact__item"><?= $contactAdmin->phone_mob?></div>
                        <? } ?>
                        <?php if ($contactAdmin->phone_home) {?>
                            <div class="title__contact__item">Телефон</div>
                            <div class="info__contact__item"><?= $contactAdmin->phone_home?></div>
                        <? } ?>
                        <?php if ($contactAdmin->fax) {?>
                            <div class="title__contact__item">Факс</div>
                            <div class="info__contact__item"><?= $contactAdmin->fax?></div>
                        <? } ?>
                        <?php if ($contactAdmin->email) {?>
                            <div class="title__contact__item">Email</div>
                            <div class="info__contact__item">
                                <a href="mailto:<?= $contactAdmin->email?>">
                                <?= $contactAdmin->email?>
                                </a>
                            </div>
                        <? } ?>
                        <?php if ($contactAdmin->skype) {?>
                            <div class="title__contact__item">Skype</div>
                            <div class="info__contact__item"><?= $contactAdmin->skype?></div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>

        <h2 class="contact__title">
            <div class="container">Региональные представители</div>
        </h2>
        <?php foreach ($contactRegionModel as $contactRegion) {?>
        <div class="contact__background">
            <div class="container">
                <div class="contact__item content__item__block__wrapper">
                    <div class="contact__item__block">
                        <div class="position">
                            <p>Страна / Область / Город</p>
                        </div>
                        <div class="contact__info">
                            <div class="name"><?= $contactRegion->contactRegionName->name?></div>
                            <div class="title__contact__item">региональный склад</div>
                            <div class="info__contact__item"><?= $contactRegion->contactRegionName->address?></div>
                        </div>
                    </div>
                    <div class="contact__item__block">
                        <div class="position">
                            <p><?= $contactRegion->position?></p>
                        </div>
                        <div class="contact__info">
                            <div class="name"><?= $contactRegion->name?></div>
                            <?php if ($contactRegion->phone_mob) {?>
                                <div class="title__contact__item">Мобильный</div>
                                <div class="info__contact__item"><?= $contactRegion->phone_mob?></div>
                            <? } ?>
                            <?php if ($contactRegion->phone_home) {?>
                                <div class="title__contact__item">Телефон</div>
                                <div class="info__contact__item"><?= $contactRegion->phone_home?></div>
                            <? } ?>
                            <?php if ($contactRegion->fax) {?>
                                <div class="title__contact__item">Факс</div>
                                <div class="info__contact__item"><?= $contactRegion->fax?></div>
                            <? } ?>
                            <?php if ($contactRegion->email) {?>
                                <div class="title__contact__item">Email</div>
                                <div class="info__contact__item">
                                    <a href="mailto:<?= $contactRegion->email?>">
                                        <?= $contactRegion->email?>
                                    </a>
                                </div>
                            <? } ?>
                            <?php if ($contactRegion->skype) {?>
                                <div class="title__contact__item">Skype</div>
                                <div class="info__contact__item"><?= $contactRegion->skype?></div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>
        <?php if (!empty($requisites->bank)) {?>
        <h2 class="contact__title">
            <div class="container">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</div>
        </h2>
        <div class="contact__background">
            <div class="container">
                <div class="contact__item">
                    <div class="position">
                        <p>Банковские реквизиты</p>
                    </div>
                    <div class="contact__info">
                        <?php if ($requisites->address_legal) {?>
                            <div class="title__contact__item">Юридический адрес</div>
                            <div class="info__contact__item"><?= $requisites->address_legal?></div>
                        <? } ?>
                        <?php if ($requisites->bic) {?>
                            <div class="title__contact__item">BIC</div>
                            <div class="info__contact__item"><?= $requisites->bic?></div>
                        <? } ?>
                        <?php if ($requisites->iban) {?>
                            <div class="title__contact__item">IBAN</div>
                            <div class="info__contact__item"><?= $requisites->iban?></div>
                        <? } ?>
                        <?php if ($requisites->bank) {?>
                            <div class="title__contact__item">Банк</div>
                            <div class="info__contact__item"><?= $requisites->bank?></div>
                        <? } ?>
                        <?php if ($requisites->address) {?>
                            <div class="title__contact__item">Адрес</div>
                            <div class="info__contact__item"><?= $requisites->address?></div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <? } ?>
        <?php
        require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer-box.php');
        ?>
    </div>
</main>
