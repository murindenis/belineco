<?php
$title = !empty($articles->seo_title)
    ? $articles->seo_title
    : '';

$keywords = !empty($articles->seo_keywords)
    ? $articles->seo_keywords
    : '';

$description = !empty($articles->seo_description)
    ? $articles->seo_description
    : '';

$this->title = $title;
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description
]);
// OG
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'property' => 'og:keywords',
    'content'  => $keywords
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => '/img/logo_sm.png'
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content'  => 'website'
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => 'http://belineco.com/'
]);
//DC
$this->registerMetaTag([
    'name'    => 'DC.creator',
    'content' => 'ООО АйТи Технолоджис Групп'
]);
$this->registerMetaTag([
    'name'    => 'DC.contributor',
    'content' => 'ESTEN / esten.co'
]);
$this->registerMetaTag([
    'name'     => 'DC:title',
    'content'  => $title
]);
$this->registerMetaTag([
    'name'    => 'DC.subject',
    'content' => $keywords
]);
$this->registerMetaTag([
    'name'    => 'DC.description',
    'content' => $description
]);
$this->registerMetaTag([
    'name'    => 'DC.language',
    'content' => 'ru-RU'
]);
$this->registerMetaTag([
    'name'    => 'DC.coverage',
    'content' => 'World'
]);
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= $articles->title?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a('Статьи', ['site/articles'], ['class' => 'breadcrumbs__item'])?>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($articles->title, ['site/articles-page', 'slug' => $articles->slug], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="/img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="news__wrapper">
        <div class="news__item__page">
            <h1 class="title__from__mobile">
                <?= $articles->title?>
            </h1>
            <div class="news__description">
                <?= $articles->description?>
            </div>
            <h1 class="title__more__news__list">Статьи</h1>
            <div class="more__news__list">
                <?php foreach ($articlesAll as $articles) {
                    echo \yii\helpers\Html::a('<div class="news__img">
                        <img src="'.$articles->getImgPath().'">
                    </div>
                    <h2>'.$articles->title.'</h2>
                    <span>'.$articles->introtext.'</span>', ['site/articles-page', 'slug' => $articles->slug], ['class' => 'little__news__item']);
                } ?>
            </div>
            <div class="button__news">
                <?= \yii\helpers\Html::a('Перейти к статьям', ['site/articles'], ['class' => 'more__news__button'])?>
            </div>
            <?php
            require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer.php');
            ?>
        </div>
    </div>
</main>
