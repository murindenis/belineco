<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/seo.php');
?>
<header class="header__page">
    <div class="logo"><a href="/"><img src="/img/logo.svg"></a></div>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <h1><?= !empty($seo->h1) ? $seo->h1 : ''?></h1>
            <div class="breadcrumbs">
                <a class="back" href="#">&lt; Вернуться назад </a>
                <a class="breadcrumbs__item" href="/">Главная</a>
                <span class="breadcrumbs__item__span">></span>
                <?= \yii\helpers\Html::a($map->name, ['site/map'], ['class' => 'breadcrumbs__item'])?>
            </div>
        </div>
    </div>
    <svg class="icon hamburger">
        <use xlink:href="img/sprite.svg#icon-menu"></use>
    </svg>
    <?php
    require_once(Yii::getAlias('@frontend').'/views/layouts/section/search.php');
    ?>
</header>
<?php
require_once(Yii::getAlias('@frontend').'/views/layouts/section/nav.php');
?>
<main class="main">
    <div class="contact__page">
        <h1 class="title__from__mobile">
            Схема проезда</h1>

            <div style="padding-top: 45px;">
                <div class="container">
                    <img src="<?= $map->getImgPath();?>" alt="<?= $map->name?>" class="map">
                </div>
            </div>


        <?php
        require_once(Yii::getAlias('@frontend').'/views/layouts/section/footer-box.php');
        ?>
    </div>
</main>
