<?php

namespace frontend\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Belineco application asset
 */
class BelinecoAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $css = [
        'belineco/css/vendor.css',
        'belineco/css/style.css',
    ];

    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',
        'belineco/js/vendor.js',
        'belineco/js/custom.js',


    ];
}
