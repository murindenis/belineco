'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

jQuery(document).ready(function () {
  $('.slider-for').slick({
    slidesToShow: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    infinite: true
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    asNavFor: '.slider-for',
    // dots: true,
    // centerMode: true,
    focusOnSelect: true,
    infinite: true,
    arrows: false
  });

  var time = 2;
  var $bar, $slick, isPause, tick, percentTime;

  $slick = $('.slider-for');

  $bar = $('.slider-progress .progress');

  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 20);
  }

  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);
      $bar.css({
        width: percentTime + "%"
      });
      if (percentTime >= 100) {
        $slick.slick('slickNext');
        startProgressbar();
      }
    }
  }

  function resetProgressbar() {
    $bar.css({
      width: 0 + '%'
    });
    clearTimeout(tick);
  }

  startProgressbar();

  $('.slick-next, .slick-prev').click(function () {
    startProgressbar();
  });

  var animElements = [].concat(_toConsumableArray(document.getElementsByClassName('section__item')));
  var windowHeight = window.innerHeight;

  $('.content__max__width').on('scroll', function (e) {

    animElements.forEach(function (item) {
      if (item.getBoundingClientRect().top < windowHeight / 1.5) {
        item.classList.add('active');
        $('.tabs__item').removeClass('active');
        $('.tabs__item a[href="#' + item.id + '"]').closest('.tabs__item').addClass('active');
      }
    });
  });

  var delta = 0;

  $('.tabs__item a').on('click', function (e) {
    e.preventDefault();
    var section = $(e.currentTarget).attr("href");
    var top = $(section).offset().top - 250;
    $('.content__max__width').animate({ scrollTop: top + delta }, 4000);
    delta += top;
  });

  $('.hamburger').on('click', function () {
    $('.nav').addClass('active');
  });
  $('.icon__close').on('click', function () {
    $('.nav').removeClass('active');
  });
  $('.icon__fixed').on('click', function () {
    $('.content__min__width').toggleClass('active');
  });
  $('.tabs__item').on('click', function () {
    $('.content__min__width').removeClass('active');
  });

  $('.search__label').on('click', function (e) {
    e.preventDefault();

    var $this = $(this),
        form = $this.closest('.search__form'),
        input = form.find('.search__input');

    input.toggleClass('show');

    $this.toggleClass('active');
  });

  // $(document).mouseup(function (e) {
  //   var container = $('.search__input');
  //   if (container.has(e.target).length === 0 && 
  //       $(e.target).closest('form').length === 0){
  //     container.removeClass('show');
  //   }
  // });

    var newOrderBtn = $('.newOrderBtn');
    var form = $('#orderForm');

    newOrderBtn.on('click', function (e) {
        e.preventDefault();
        if ($('#name').val() != '' && $('#phone').val() != '') {
            $.ajax({
                type: 'post',
                data: form.serialize(),
                url: '/site/order-ajax',
                success: function (data) {
                    document.getElementById('orderForm').reset();
                    alert('Вы сделали заказ!');
                },
            });
        } else {
            $('.formMsg').css('display','block');
            $('.formMsg').html('Поля имя и телефон обязательны к заполнению!').delay(3000).fadeOut();
        }
    });

    $('.back').on('click', function (e) {
        e.preventDefault();
        history.back();
    });

    $('.icon_search').on('click', function () {
        $('.pop-up').addClass('active');
    });
    $('.icon_close').on('click', function () {
        $('.pop-up').removeClass('active');
    });
});