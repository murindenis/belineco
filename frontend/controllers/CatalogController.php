<?php

namespace frontend\controllers;

use common\models\Product;
use common\models\ProductCategory;
use common\models\ProductOption;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class CatalogController extends Controller
{
    public function actionIndex($slug)
    {
        $category = ProductCategory::find()->where(['slug' => $slug])->one();
        $brands = $category->productBrands;

        return $this->render('index', [
            'category' => $category,
            'brands'   => $brands,
        ]);
    }

    public function actionPage($slug)
    {
        $product = Product::find()->where(['slug' => $slug])->one();
        $productOptions = ProductOption::find()->where(['product_id' => $product->id])->all();

        return $this->render('page', [
            'product' => $product,
            'productOptions' => $productOptions,
        ]);
    }
}
