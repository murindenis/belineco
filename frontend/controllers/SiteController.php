<?php

namespace frontend\controllers;

use common\models\AboutCategory;
use common\models\AboutOurPurposeAndMission;
use common\models\AboutPhoto;
use common\models\AboutProduct;
use common\models\AboutWhatDoWeOffer;
use common\models\AboutWhoAreWe;
use common\models\Articles;
use common\models\ContactAdmin;
use common\models\ContactRegion;
use common\models\DealerCategory;
use common\models\Email;
use common\models\Map;
use common\models\News;
use common\models\Order;
use common\models\Product;
use common\models\Region;
use common\models\Requisites;
use frontend\models\ContactForm;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $products = Product::find()->where(['index' => true])->all();
        return $this->render('index', [
            'products' => $products,
        ]);
    }

    public function actionMaps()
    {
        $map = Map::find()->where(['id' => 1])->one();
        return $this->render('maps', [
            'map' => $map,
        ]);
    }

    public function actionSearch($search)
    {
        $products = Product::find()->andFilterWhere(['like', 'name', $search])->all();
        return $this->render('search', [
            'products' => $products,
        ]);
    }

    public function actionWhereBuy()
    {
        $regions = Region::find()->all();
        return $this->render('where-buy', [
            'regions' => $regions
        ]);
    }

    public function actionNews()
    {
        $lastNews = News::find()->orderBy(['id' => SORT_DESC])->one();

        $query = News::find()->orderBy(['id' => SORT_DESC])->andWhere(['<>', 'id', $lastNews->id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
        $news = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('news', [
            'news'  => $news,
            'lastNews' => $lastNews,
            'pages' => $pages,
        ]);
    }

    public function actionArticles()
    {
        $lastArticle = Articles::find()->orderBy(['id' => SORT_DESC])->one();

        $query = Articles::find()->orderBy(['id' => SORT_DESC])->andWhere(['<>', 'id', $lastArticle->id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
        $article = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('articles', [
            'article'  => $article,
            'lastArticle' => $lastArticle,
            'pages' => $pages,
        ]);
    }

    public function actionAbouts()
    {
        $aboutCategories = AboutCategory::find()->all();
        $block1 = AboutWhoAreWe::find()->where(['id' => 1])->one();
        $block2 = AboutWhatDoWeOffer::find()->where(['id' => 1])->one();
        $block3 = AboutProduct::find()->where(['id' => 1])->one();
        $block4 = AboutOurPurposeAndMission::find()->where(['id' => 1])->one();
        $photos = AboutPhoto::find()->all();

        return $this->render('abouts', [
            'aboutCategories'  => $aboutCategories,
            'block1'  => $block1,
            'block2'  => $block2,
            'block3'  => $block3,
            'block4'  => $block4,
            'photos'  => $photos,
        ]);
    }

    public function actionDealers()
    {
        $dealerCategories = DealerCategory::find()->all();
        return $this->render('dealers', [
            'dealerCategories' => $dealerCategories
        ]);
    }

    public function actionNewsPage($slug)
    {
        $news = News::find()->where(['slug' => $slug])->one();
        $newsAll = News::find()->orderBy(['id' => SORT_DESC])->limit(4)->all();
        return $this->render('news-page', [
            'news'    => $news,
            'newsAll' => $newsAll,
        ]);
    }

    public function actionArticlesPage($slug)
    {
        $articles = Articles::find()->where(['slug' => $slug])->one();
        $articlesAll = Articles::find()->orderBy(['id' => SORT_DESC])->limit(4)->all();
        return $this->render('articles-page', [
            'articles'    => $articles,
            'articlesAll' => $articlesAll,
        ]);
    }

    public function actionContacts()
    {
        $contactAdminModel  = ContactAdmin::find()->all();
        $contactRegionModel = ContactRegion::find()->all();
        $requisites = Requisites::find()->where(['id' => 1])->one();
        return $this->render('contacts', [
            'contactAdminModel'  => $contactAdminModel,
            'contactRegionModel' => $contactRegionModel,
            'requisites' => $requisites,
        ]);
    }

    public function actionServices()
    {
        return $this->render('services', []);
    }

    public function actionOrderAjax()
    {
        $firstname = Yii::$app->request->post('firstname');
        $lastname  = Yii::$app->request->post('lastname');
        $phone     = Yii::$app->request->post('phone');
        $country   = Yii::$app->request->post('country');
        $firm      = Yii::$app->request->post('firm');
        $position  = Yii::$app->request->post('position');
        $email     = Yii::$app->request->post('email');
        $product_id= Yii::$app->request->post('product_id');

        $order = new Order();
        $order->firstname = $firstname;
        $order->lastname = $lastname;
        $order->phone = $phone;
        $order->country = $country;
        $order->firm = $firm;
        $order->position = $position;
        $order->email = $email;
        $order->product_id = $product_id;
        $order->save();

        $product = Product::find()->where(['id' => $product_id])->one();

        $emailModel = Email::find()->where(['id' => 1])->one();
        $subject = 'Новый заказ';
        $msg = '<b>'.date('d-m-Y H:i:s').'</b>- поступил новый заказ<br/>'.
            'Имя: '.$firstname.' '.$lastname.'<br/>'.
            'Телефон: '.$phone.'<br/>'.
            'Страна: '.$country.'<br/>'.
            'Фирма: '.$firm.'<br/>'.
            'Должность: '.$position.'<br/>'.
            'Email:'.$email.'<br/>';
            'Товар:'.$product->name.'<br/>';
        mail($emailModel->email, $subject, $msg);
    }
}
